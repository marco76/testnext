import styles from "../styles/keyfacts.module.scss";
import Grid from '@mui/material/Grid';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck, faGauge, faUsers, faGasPump, faGears, faChevronRight } from '@fortawesome/pro-light-svg-icons';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';

const Keyfacts = (props) => {
    const theme = useTheme();
    const isXs = useMediaQuery(theme.breakpoints.down('xs'));
    const isSm = useMediaQuery(theme.breakpoints.down('sm'));

    const spacing = isXs ? 0 : isSm ? 1 : 2;

    return (
        <Grid container spacing={spacing} className={styles.allKeyFacts}>
            <Grid item xs={6} sm={3} className={styles.singleKeyFact}>
                <div>
                    <FontAwesomeIcon icon={faGauge} />
                    <h3>Leistung</h3>
                    <p>{props.carData.hp}</p>
                </div>
            </Grid>
            <Grid item xs={6} sm={3} className={styles.singleKeyFact}>
                <div>
                    <FontAwesomeIcon icon={faGasPump} />
                    <h3>Treibstoff</h3>
                    <p>{props.carData.fuel}</p>
                </div>
            </Grid>
            <Grid item xs={6} sm={3} className={styles.singleKeyFact}>
                <div>
                    <FontAwesomeIcon icon={faGears} />
                    <h3>Getriebe</h3>
                    <p>{props.carData.gear}</p>
                </div>
            </Grid>
            <Grid item xs={6} sm={3} className={styles.singleKeyFact}>
                <div>
                    <FontAwesomeIcon icon={faUsers} />
                    <h3>Sitze</h3>
                    <p>{props.carData.seats}</p>
                </div>
            </Grid>
        </Grid>
    )
}

export default Keyfacts;