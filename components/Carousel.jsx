import React, { useState, useEffect } from 'react';
import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import styles from "../styles/image_carousel.module.scss";

const ImageCarousel = ({ car, setIsCarousel }) => {
    const [validImages, setValidImages] = useState([]);

    useEffect(() => {
        // Direkt die URLs aus dem car-Objekt extrahieren, ohne die Existenz zu prüfen
        const imageKeys = Object.keys(car).filter(key => key.startsWith('imagepath'));
        const images = imageKeys.map(key => car[key]);

        // Überprüfe, ob mehr als vier Bilder existieren
        if (images.length > 4) {
            setValidImages(images);
        } else {
            setIsCarousel(false); // Deaktiviere das Carousel
        }
    }, [car, setIsCarousel]);

    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 3, // Stelle sicher, dass dies zu deinem Layout passt
        slidesToScroll: 1,
        autoplay: true,
        responsive: [
            {
                breakpoint: 768, // Definiert den Breakpoint für Bildschirme bis 768px Breite
                settings: {
                    slidesToShow: 1, // Zeigt nur 1 Bild für Bildschirme unter 768px Breite
                    slidesToScroll: 1,
                }
            },
            // Sie können weitere Breakpoints hinzufügen, falls nötig
        ] // Korrigiere den Tippfehler von "autpoplay" zu "autoplay"
    };

    return (
        <div>
            {validImages.length > 0 && (
                <Slider {...settings}>
                    {validImages.map((imgSrc, index) => (
                        <div key={index} className="slideContainer">
                            <div className="slideContainerInner">
                                <img src={imgSrc} alt={`Slide ${index}`} />
                            </div>
                        </div>
                    ))}
                </Slider>

            )}
        </div>
    );
};

export default ImageCarousel;
