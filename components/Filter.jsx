import React, { useState } from 'react';
import Image from 'next/image';
import Grid from '@mui/material/Grid';
import styles from '../styles/filter.module.scss'
import Button from '@mui/material/Button';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';


const Filter = (props) => {

  const theme = useTheme();
  const isXs = useMediaQuery(theme.breakpoints.down('xs'));
  const isSm = useMediaQuery(theme.breakpoints.down('sm'));

  const spacing = isXs ? 0 : isSm ? 1 : 2;

  const [selected, setSelected] = useState({});
  const [selectedString, setSelectedString] = useState({});

  const handleSelectString = (selectionKey) => {
    let filterParts = selectionKey.split("-");
    props.filterAction(filterParts[0] + '-' + filterParts[1]);
    setSelectedString(prevSelected => ({
      ...prevSelected,
      [selectionKey]: !prevSelected[selectionKey]
    }));
  };

  const handleSelect = (key, value) => {

    props.filterAction(key + '-' + value);

    // Toggle-Logik für die Auswahl
    const newSelected = { ...selected };
    const selectionKey = `${key}-${value}`;
    newSelected[selectionKey] = !newSelected[selectionKey];
    setSelected(newSelected);
  };

  const getImagePath = (key, value) => {
    let fileName = value.replace(/\s+/g, '').toLowerCase();
    const basePath = key === 'producer' ? '/images/brand-logos/' : '/images/icons/';
    return `${basePath}${fileName}.png`;
  };


  const isImageCategory = (key) => {
    return key === 'producer' || key === 'type';
  };

  const isSubtextCategory = (key) => {
    return key === 'type';
  };

  const titleMapping = {
    producer: 'Nach Marken filtern',
    type: 'Nach Bauart filtern',
    doors: 'Nach Türen filtern',
    drivetype: 'Nach Antriebsart filtern',
    fuel: 'Nach Kraftstoff filtern',
    gear: 'Nach Getriebe filtern',
    seats: 'Nach Sitzen filtern'
  };


  return (
    <div className={`${styles.filterContainerInner} ${props.isFilterVisible ? styles.filterVisible : ''}`}>
      {Object.entries(props.filterData).map(([key, values]) => (
        <div key={key}>
          <h3 className={styles.headline}>{titleMapping[key]} </h3>
          {isImageCategory(key) ? (
            <Grid container spacing={2}>
              {values.map(value => (
                <Grid item xs={4} sm={2} md={4} key={value}>
                  <div
                    className={`${styles.image_filter} ${key === 'producer' ? styles.producerImage : ''} ${selected[`${key}-${value}`] ? styles.selected : ''}`}
                    onClick={() => handleSelect(key, value)}
                  >
                    <Image
                      src={getImagePath(key, value)}
                      alt={value}
                      width={100}
                      height={50}
                      className={styles.filterImage}
                    />
                    {isSubtextCategory(key) && <span className={styles.iconSubtext}>{value}</span>}
                  </div>
                </Grid>
              ))}
            </Grid>
          ) : (
            <Grid container spacing={2}>
              {values.map((value, index) => {
                const selectionKey = `${key}-${value}-${index}`; // Eindeutiger Schlüssel
                return (
                  <Grid item xs={4} sm={2} md={4} key={selectionKey}>
                    <div className={`${styles.string_filter} ${selectedString[selectionKey] ? styles.selected : ''}`}
                      onClick={() => handleSelectString(selectionKey)}>
                      {value}
                    </div>
                  </Grid>
                );
              })}
            </Grid>
          )}
        </div>
      ))}
      <div className={styles.filterCloseBtn}>
        <Button variant="contained" color="primary" onClick={() => props.filterClose(false)}>
          Filter anwenden
        </Button>
      </div>
    </div>

  );

}

export default Filter;