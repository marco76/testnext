import styles from "../styles/carfacts.module.scss";
import Grid from '@mui/material/Grid';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDroplet, faCar, faCarSide, faCalendarDays, faCircleParking, faRoute } from '@fortawesome/pro-light-svg-icons';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';

const CarFacts = (props) => {
    const theme = useTheme();
    const isXs = useMediaQuery(theme.breakpoints.down('xs'));
    const isSm = useMediaQuery(theme.breakpoints.down('sm'));
  
    // Bestimme das Spacing basierend auf der Bildschirmgröße
    const spacing = isXs ? 0 : isSm ? 1 : 2;

    return (
        <Grid container spacing={spacing}  className={styles.allCarFacts}>
            <Grid item xs={6} sm={2} className={styles.singleCarFact}>
                <div>
                    <FontAwesomeIcon icon={faCar} />
                    <h5>Bauart</h5>
                    <p>{props.carData.type}</p>
                </div>
            </Grid>
            <Grid item xs={6} sm={2} className={styles.singleCarFact}>
                <div>
                    <FontAwesomeIcon icon={faCarSide} />
                    <h5>Türen</h5>
                    <p>{props.carData.doors}</p>
                </div>
            </Grid>
            <Grid item xs={6} sm={2} className={styles.singleCarFact}>
                <div>
                    <FontAwesomeIcon icon={faDroplet} />
                    <h5>Farbe</h5>
                    <p>{props.carData.color}</p>
                </div>
            </Grid>
            <Grid item xs={6} sm={2} className={styles.singleCarFact}>
                <div>
                    <FontAwesomeIcon icon={faCalendarDays} />
                    <h5>Baujahr</h5>
                    <p>{props.carData.yearofconstruction}</p>
                </div>
            </Grid>
            <Grid item xs={6} sm={2} className={styles.singleCarFact}>
                <div>
                <FontAwesomeIcon icon={faCircleParking} />
                    <h5>Parkassistent hinten & vorne</h5>
                    <p>{props.carData.parkingassistantfront}</p>
                </div>
            </Grid>
            <Grid item xs={6} sm={2} className={styles.singleCarFact}>
                <div>
                <FontAwesomeIcon icon={faRoute} />
                    <h5>Navi</h5>
                    <p>{props.carData.navigationsystem}</p>
                </div>
            </Grid>
     
        </Grid>
    )
}

export default CarFacts;