import styles from "../styles/other_cars.module.scss";
import { fetchCarDatafromApp } from '../pages//api/cars';
import Grid from '@mui/material/Grid';
import CircularProgress from '@mui/material/CircularProgress';
import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck, faGauge, faUsers, faGasPump, faGears, faChevronRight } from '@fortawesome/pro-light-svg-icons';
import Image from "next/image";
import Button from '@mui/material/Button';
import Link from "next/link";

const OtherCars = (props) => {
    const [carData, setCarData] = useState(null);
    const [loading, setLoading] = useState(true);
    const currentCarId = props.carid;

    const beautifyUrl = (text) => {
        return text
            .toLowerCase()
            .replace(/ä/g, 'ae')
            .replace(/ö/g, 'oe')
            .replace(/ü/g, 'ue')
            .replace(/ß/g, 'ss')
            .replace(/\s+/g, '-')
            .replace(/[^a-z0-9-]/g, '');
    };

    useEffect(() => {
        const fetchData = async () => {
            setLoading(true);
            try {
                const carDataArray = await fetchCarDatafromApp();
                // Filtere das aktuelle Auto heraus und wähle zufällig 3 andere Fahrzeuge aus
                const filteredCars = carDataArray.filter(car => car.carid !== currentCarId);
                const randomCars = filteredCars.sort(() => 0.5 - Math.random()).slice(0, 3);
                setCarData(randomCars);
            } catch (error) {
                console.error('Error fetching car data:', error);
            } finally {
                setLoading(false);
            }
        };
        fetchData();
    }, [currentCarId]);


    return (
        <div>
            {loading ? (
                <CircularProgress />
            ) : (
                <Grid container spacing={2}>
                    {carData.map((car, index) => (
                        <Grid item xs={12} sm={4} key={index}>
                            <div className={styles.carCard}>
                                <div className={styles.imageContainer}>
                                <Image src={car.imagepath} width={400} height={400} alt={`${car.producer} ${car.vehicletype}`} />
                                </div>
                                <h3>{car.producer} {car.vehicletype}</h3>
                                <Grid container spacing={1} className={styles.factContainer}>
                                    <Grid item xs={3} className={styles.singleFact}>
                                        <div className={styles.trafefacticon}>
                                            <FontAwesomeIcon icon={faGauge} />
                                        </div>
                                        {car.hp}
                                    </Grid>
                                    <Grid item xs={3} className={styles.singleFact}>
                                        <div className={styles.trafefacticon}>
                                            <FontAwesomeIcon icon={faUsers} />
                                        </div>
                                        {car.seats}
                                    </Grid>
                                    <Grid item xs={3} className={styles.singleFact}>
                                        <div className={styles.trafefacticon}>
                                            <FontAwesomeIcon icon={faGasPump} />
                                        </div>
                                        {car.fuel}
                                    </Grid>
                                    <Grid item xs={3} className={styles.singleFact}>
                                        <div className={styles.trafefacticon}>
                                            <FontAwesomeIcon icon={faGears} />
                                        </div>
                                        {car.gear}
                                    </Grid>
                                </Grid>
                                <Link
                                    href={`/fuhrpark/${beautifyUrl(car.producer)}-${beautifyUrl(car.vehicletype)}?carid=${car.carid}`}
                                    passHref
                                >
                                    <Button variant="contained" color="primary">
                                        Details
                                    </Button>
                                </Link>
                            </div>
                        </Grid>
                    ))}
                </Grid>
            )}
        </div>
    )
}

export default OtherCars;