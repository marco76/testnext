import React, { useEffect, useState } from 'react';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Header from './Header';
import Footer from './Footer';
import config from '@/config';
import styles from "../styles/layout.module.scss";
import { SpeedInsights } from "@vercel/speed-insights/next"
import { Analytics } from "@vercel/analytics/react"


function Layout({ children }) {
    const [theme, setTheme] = useState(createTheme({
        palette: {
            primary: {
                main: '#333',
            },
            secondary: {
                main: '#999',
            },
        },
        components: {
            // Konfiguration für MUI Button
            MuiButton: {
                styleOverrides: {
                    // Stile für die "root"-Variante des Buttons
                    root: {
                        color: 'white', // Setzt die Textfarbe auf Weiß
                    },
                },
            },
        },
    }));

    // Konvertierungsfunktion: Hex zu RGB
    const hexToRgb = (hex) => {
        let r = 0, g = 0, b = 0;
        // 3 Zeichen lang oder 6 Zeichen lang
        if (hex.length === 4) {
            r = parseInt(hex[1] + hex[1], 16);
            g = parseInt(hex[2] + hex[2], 16);
            b = parseInt(hex[3] + hex[3], 16);
        } else if (hex.length === 7) {
            r = parseInt(hex[1] + hex[2], 16);
            g = parseInt(hex[3] + hex[4], 16);
            b = parseInt(hex[5] + hex[6], 16);
        }
        return [r, g, b];
    };

    useEffect(() => {
        fetch(`${config.apiUrl}/settings/design`)
            .then(response => response.json())
            .then(data => {
                const colors = data[0]; 
                const primaryColor = colors.primarycolorhex || '#1976d2';
                const secondaryColor = colors.secondcolorhex || '#dc004e';

                // Konvertiere primaryColor von Hex zu RGB
                const [r, g, b] = hexToRgb(primaryColor);

                const newTheme = createTheme({
                    palette: {
                        primary: {
                            main: primaryColor,
                        },
                        secondary: {
                            main: secondaryColor,
                        },
                    },
                    components: {
                        // Konfiguration für MUI Button
                        MuiButton: {
                            styleOverrides: {
                                // Stile für die "root"-Variante des Buttons
                                root: {
                                    color: 'white', // Setzt die Textfarbe auf Weiß
                                },
                            },
                        },
                    },
                });

                setTheme(newTheme);
                document.documentElement.style.setProperty('--primary-color', primaryColor);
                document.documentElement.style.setProperty('--secondary-color', secondaryColor);
                // Setze die primary Farbe mit Opazität
                document.documentElement.style.setProperty('--primary-color-opacity', `rgba(${r}, ${g}, ${b}, 0.1)`);
                document.documentElement.style.setProperty('--white', '#fff');
                document.documentElement.style.setProperty('--grey', '#ccc');
            })
            .catch(error => {
                console.error("Fehler beim Laden der Farben: ", error);
            });
    }, []);

    return (
        <ThemeProvider theme={theme}>
            <div>
                <Header />
                <div>{children}</div>
                <SpeedInsights />
                <Analytics />
                <Footer />
            </div>
        </ThemeProvider>
    );
}

export default Layout;
