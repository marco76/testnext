import React from 'react';
import Image from 'next/image';

import styles from "../styles/orderCar.module.scss";

const OrderCar = (props) => {

    return (
        <div className={styles.container}>
            <h4>Angefragtes Fahrzeug</h4>
            <div className={styles.imageContainer}>
            <Image src={props.car.imagepath} width={500} height={500} alt={props.car.producer+' '+props.car.vehicletype}/>
            </div>
            <h2>{props.car.producer} {props.car.vehicletype}</h2>
            <h3>{props.car.type}</h3>
        </div>
    )
}

export default OrderCar;