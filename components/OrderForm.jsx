import React, { useState, useEffect } from 'react';
import { Grid, TextField, Checkbox, Button, FormControlLabel, Typography } from '@mui/material';
import Datepicker from '@/components/Datepicker';
import styles from "../styles/orderForm.module.scss";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { useRouter } from 'next/router';
import config from '@/config';
import Link from 'next/link';



const validationSchema = yup.object({
    mietzeitraum: yup.string().required('Mietzeitraum ist erforderlich'),
    abholzeit: yup.string().required('Abholzeit ist erforderlich'),
    rueckgabezeit: yup.string().required('Rückgabezeit ist erforderlich'),
    vorname: yup.string().required('Vorname ist erforderlich'),
    name: yup.string().required('Name ist erforderlich'),
    benoetigteKilometer: yup.string().required('Benötigte Kilometer sind erforderlich'),
    emailadresse: yup.string().email('Ungültige Emailadresse').required('Emailadresse ist erforderlich'),
    telefonnummer: yup.string().required('Telefonnummer ist erforderlich'),
    agbAkzeptieren: yup.bool().oneOf([true], 'Die AGBs und Datenschutzbestimmungen müssen akzeptiert werden'),
}).required();



const OrderForm = () => {
    const [dateRange, setDateRange] = useState('');
    const [startDatum, setStartDatum] = useState('');
    const [endDatum, setEndDatum] = useState('');
    const [pickerStatus, setPickerStatus] = useState(false);
    const [formSubmitted, setFormSubmitted] = useState(false);
    const [successMessage, setSuccessMessage] = useState('');
    const router = useRouter();
    const { carid } = router.query;
    const { register, handleSubmit, formState: { errors }, setValue } = useForm({
        resolver: yupResolver(validationSchema)
    });


    const onSubmit = data => {
        fetch('/api/sendRequest', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        })
            .then(response => response.json())
            .then(data => {
           
                setSuccessMessage('Deine Anfrage wurde erfolgreich versandt!'); // Setze Erfolgsmeldung
                setFormSubmitted(true); // Setze, dass das Formular abgesendet wurde
                localStorage.clear(); // Lösche den gesamten Local Storage
                reset(); // Setzt das Formular zurück
            })
            .catch((error) => {
                console.error('Fehler:', error);
            });
    };


    useEffect(() => {
        updateDateRangeFromStorage();
    }, []);

    const handlePickerInputClick = () => setPickerStatus(true);
    const hideDatepicker = () => setPickerStatus(false);

    const updateDateRangeFromStorage = () => {
        const storedStartDateStr = localStorage.getItem('startDate');
        const storedEndDateStr = localStorage.getItem('endDate');

        let storedStartDate = null;
        let storedEndDate = null;

        if (storedStartDateStr) {
            const parsedStartDate = JSON.parse(storedStartDateStr);
            if (new Date().getTime() < parsedStartDate.expiry) {
                storedStartDate = parsedStartDate.value;
            } else {
                localStorage.removeItem('startDate'); // Daten sind abgelaufen und werden entfernt
            }
        }

        if (storedEndDateStr) {
            const parsedEndDate = JSON.parse(storedEndDateStr);
            if (new Date().getTime() < parsedEndDate.expiry) {
                storedEndDate = parsedEndDate.value;
            } else {
                localStorage.removeItem('endDate'); // Daten sind abgelaufen und werden entfernt
            }
        }
        if (storedStartDate && storedEndDate) {
            const range = `${storedStartDate} - ${storedEndDate}`;
            setDateRange(range);
            setStartDatum(storedStartDate);
            setEndDatum(storedEndDate);
            // Aktualisieren Sie hier den Wert für mietzeitraum im Formularzustand
            setValue('mietzeitraum', range, { shouldValidate: true });
            setValue('abholdatum', storedStartDate, { shouldValidate: true });
            setValue('rueckgabedatum', storedEndDate, { shouldValidate: true });
            setValue('carid', carid, { shouldValidate: true });
        } else {
            setDateRange('');
            setStartDatum('');
            setEndDatum('');
            // Stellen Sie sicher, dass der Zustand zurückgesetzt wird, wenn keine Daten vorhanden sind
            setValue('mietzeitraum', '', { shouldValidate: true });
            setValue('abholdatum', '', { shouldValidate: true });
            setValue('rueckgabedatum', '', { shouldValidate: true });
            setValue('carid', carid, { shouldValidate: true });
        }
    };

    return (
        <>
            {pickerStatus === true &&
                <div className={styles.datepickerLayer}>
                    <div className={styles.inner}>
                        <Datepicker hideDatepicker={hideDatepicker} updateDateRange={updateDateRangeFromStorage} />
                    </div>
                </div>
            }
            {!formSubmitted && (
                <form onSubmit={handleSubmit(onSubmit)}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                label="Mietzeitraum"
                                {...register('mietzeitraum')}
                                error={!!errors.mietzeitraum}
                                helperText={errors.mietzeitraum?.message || ''}
                                onClick={handlePickerInputClick}
                                value={dateRange}
                                InputLabelProps={{ shrink: true }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                fullWidth
                                label="Abholzeit"
                                type="time"
                                {...register('abholzeit')}
                                error={!!errors.abholzeit}
                                helperText={errors.abholzeit?.message || ''}
                                InputLabelProps={{ shrink: true }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                fullWidth
                                label="Rückgabezeit"
                                type="time"
                                {...register('rueckgabezeit')}
                                error={!!errors.rueckgabezeit}
                                helperText={errors.rueckgabezeit?.message || ''}
                                InputLabelProps={{ shrink: true }}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                label="Vorname"
                                {...register('vorname')}
                                error={!!errors.vorname}
                                helperText={errors.vorname?.message || ''}
                            />

                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                label="Name"
                                {...register('name')}
                                error={!!errors.name}
                                helperText={errors.name?.message || ''}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                label="Firma"
                                {...register('firma')}
                                error={!!errors.firma}
                                helperText={errors.firma?.message || ''}
                            />

                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                label="Benötigte Kilometer"
                                type="number"
                                {...register('benoetigteKilometer')}
                                error={!!errors.benoetigteKilometer}
                                helperText={errors.benoetigteKilometer?.message || ''}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                label="Emailadresse"
                                type="email"
                                {...register('emailadresse')}
                                error={!!errors.emailadresse}
                                helperText={errors.emailadresse?.message || ''}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                label="Telefonnummer"
                                type="tel"
                                {...register('telefonnummer')}
                                error={!!errors.telefonnummer}
                                helperText={errors.telefonnummer?.message || ''}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                label="Bemerkungen"
                                {...register('bemerkungen')}
                                multiline
                                rows={3}
                                variant="outlined"
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <FormControlLabel
                                control={<Checkbox {...register('agbAkzeptieren')} />}
                                label="Mit der Nutzung des Formulars erklärst du dich mit der Speicherung und Verarbeitung deiner Daten durch diese Webseite gemäß unserer Datenschutzerklärung einverstanden."
                            />
                            {errors.agbAkzeptieren && (
                                <Typography color="error" variant="body2">
                                    {errors.agbAkzeptieren.message}
                                </Typography>
                            )}
                        </Grid>

                        <Grid item xs={12}>
                            <Button variant="contained" color="primary" type="submit">
                                Fahrzeug jetzt anfragen
                            </Button>
                        </Grid>
                    </Grid>
                    <input
                        type="hidden"
                        name="abholdatum"
                        value={startDatum}
                    />
                    <input
                        type="hidden"
                        name="rueckgabedatum"
                        value={endDatum}
                    />
                    <input
                        type="hidden"
                        name="carid"
                        value={carid}
                    />

                </form>
            )}
            {formSubmitted && (
                <div className={styles.successMessageWrapper}>
                    <h1 className={styles.successHeadline}>Die Anfrage wurde erfolgreich versendet</h1>
                    <p className={styles.successText}>Vielen Dank für Ihr Interesse an unseren Fahrzeugen. Ihre Anfrage wurde erfolgreich an uns versendet und wird umgehend von uns bearbeitet. Sollten Sie Fragen oder Änderungswünsche haben, stehen wir ihnen gerne telefonisch unter <Link href={'tel:'+`${config.hotlineLink}`} alt="Rufen Sie uns an">{config.hotline}</Link> oder via Mail unter der E-Mailadresse <Link href={'mailto:'+`${config.mailaddress}`} alt="Schreiben Sie uns eine Mail">{config.mailaddress}</Link> zu Ihrer Verfügung.</p>
                    <p>Ihr {config.teamname}</p>
                </div>
            )}
        </>
    )
}

export default OrderForm;