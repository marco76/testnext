// theme.js
import { createTheme } from '@mui/material/styles';

const adminTheme = createTheme({
  palette: {
      primary: {
          main: '#53b574', // Hier setzen Sie Ihre bevorzugte primäre Farbe
      },
      secondary: {
          main: '#999999'
      },
  },
  components: {
      MuiButton: {
          styleOverrides: {
              root: {
                  // Grundstil für den Button
                  color: 'white', // Schriftfarbe
                  '&:hover': {
                      // Hover-Stil
                      backgroundColor: '#3db353', // Beispiel für eine leicht dunklere Farbe
                  },
              },
          },
      },
  },
});

export default adminTheme;
