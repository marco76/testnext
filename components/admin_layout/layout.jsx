import React, { useState } from 'react';
import { AppBar, Box, Toolbar, IconButton, Drawer, List, ListItem, ListItemText, CssBaseline, Typography, Button, useTheme, useMediaQuery } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import adminTheme from './theme';
import styles from '../../styles/admin_layout.module.scss'

function DashboardLayout({ children }) {
    const [mobileOpen, setMobileOpen] = useState(false);
    const muiTheme = useTheme();
    const isMobile = useMediaQuery(muiTheme.breakpoints.down('sm'));

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    const menuItems = {
        'dashboard': 'Dashboard',
        'dispo': 'Dispo',
        'carsoverview': 'Fahrzeugübersicht',
        'settings': 'Einstellungen'
    };

    const drawer = (
        <List>
            {Object.entries(menuItems).map(([key, value]) => (
                <ListItem button key={key} component="a" href={`${key.toLowerCase()}`}>
                    <ListItemText primary={value} />
                </ListItem>
            ))}
        </List>
    );
    return (
        <div style={{ display: 'flex' }}>


                <CssBaseline />
                <AppBar position="fixed" sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }}>
                    <Toolbar>
                        {isMobile && (
                            <IconButton
                                color="inherit"
                                aria-label="open drawer"
                                edge="start"
                                onClick={handleDrawerToggle}
                                sx={{ mr: 2 }}
                            >
                                <MenuIcon />
                            </IconButton>
                        )}

                        <Typography variant="h6" component="div" sx={{ flexGrow: 1, color: 'white', textAlign: 'center' }}>
                            Trafe Admin
                        </Typography>
                        <Button color="inherit" onClick={() => alert('Logout')}>Logout</Button>
                        {/* Füge leere Box hinzu, um den Logout-Button nach rechts zu schieben, falls notwendig */}
                        {isMobile && <Box sx={{ flexGrow: 1 }} />}
                    </Toolbar>
                </AppBar>

                <Drawer className={styles.sidemenu}
                    variant={isMobile ? 'temporary' : 'permanent'}
                    open={mobileOpen}
                    onClose={handleDrawerToggle}
                    ModalProps={{
                        keepMounted: true, // Better open performance on mobile.
                    }}
                    sx={{
                        width: 240,
                        flexShrink: 0,
                        [`& .MuiDrawer-paper`]: { width: 240, boxSizing: 'border-box', marginTop: '70px' },
                    }}
                >
                    {drawer}
                </Drawer>
                <div style={{ flexGrow: 1, padding: adminTheme.spacing(3) }}>
                    <Toolbar /> {/* Sorgt für den nötigen Abstand unterhalb der AppBar */}
                    <div className={styles.adminContent}>
                    {children}
                    </div>
                </div>

 

        </div>
    );
}



DashboardLayout.getLayout = function getLayout(page) {
    return page;
}

export default DashboardLayout;
