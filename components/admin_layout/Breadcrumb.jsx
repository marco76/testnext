import { Breadcrumbs, Link, Typography } from '@mui/material';
import { useRouter } from 'next/router';

const DynamicBreadcrumbs = ({ menuItems }) => {
  const router = useRouter();
  const pathnames = router.pathname.split('/').filter(x => x);

  return (
    <Breadcrumbs aria-label="breadcrumb">
      {pathnames.map((value, index) => {
        const last = index === pathnames.length - 1;
        const to = `/${pathnames.slice(0, index + 1).join('/')}`;
        const label = menuItems[value] || value; // Verwende das Label aus menuItems oder den URL-Wert

        return last ? (
          <Typography color="text.primary" key={to}>{label}</Typography>
        ) : (
          <Link color="inherit" href={to} key={to}>
            {label}
          </Link>
        );
      })}
    </Breadcrumbs>
  );
};
