import '@mobiscroll/react/dist/css/mobiscroll.min.css';
import styles from "../styles/datepicker.module.scss";
import { Datepicker, Page, setOptions, localeDe } from '@mobiscroll/react';
import { useMemo } from 'react';
import React, { useState, useEffect } from 'react';
import Button from '@mui/material/Button';
import axios from 'axios';

setOptions({
    locale: localeDe,
    theme: 'material',
    themeVariant: 'light'
});

const TrafeDatepicker = (props) => {
    const [calendarPages, setcalendarPages] = useState(3);
    const twoYearsFromNow = new Date();
    const [showConfirmButton, setShowConfirmButton] = useState(false);
    const [invalidDays, setInvalidDays] = useState([]);

    useEffect(() => {
        const fetchHolidays = async () => {
            try {
                const response = await axios.get('https://get.api-feiertage.de/?states=by');
                if (response.data && response.data.feiertage) {
                    // Konvertiere Feiertage in das erforderliche Format für den Datepicker
                    const holidays = response.data.feiertage.map(holiday => holiday.date);
                    setInvalidDays(holidays);
                }
            } catch (error) {
                console.error('Error fetching holiday data:', error);
            }
        };
        fetchHolidays();
    }, []);

    useEffect(() => {
        const updatePagesBasedOnWidth = () => {
            const width = window.innerWidth;

            if (width < 600) { // xs: weniger als 600px
                setcalendarPages(1);
            } else if (width >= 600 && width < 960) { // md: 600px bis unter 960px
                setcalendarPages(2);
            } else { // lg und größer: 960px und mehr
                setcalendarPages(3);
            }
        };

        // Führe die Funktion beim ersten Rendern und bei jeder Änderung der Fenstergröße aus
        updatePagesBasedOnWidth();
        window.addEventListener('resize', updatePagesBasedOnWidth);

        // Entferne den Event-Listener, wenn die Komponente demontiert wird, um Memory Leaks zu vermeiden
        return () => window.removeEventListener('resize', updatePagesBasedOnWidth);
    }, []);

    useEffect(() => {
        // Prüfung auf Ablauf beim Initialisieren der Komponente
        checkExpiry();
        // Existierender useEffect für fetchHolidays...
    }, []);

    // Prüfung auf Ablauf und Entfernen abgelaufener Daten
    const checkExpiry = () => {
        const startDateStr = localStorage.getItem('startDate');
        const endDateStr = localStorage.getItem('endDate');
        const now = new Date();

        if (startDateStr && endDateStr) {
            const { value: startDateValue, expiry: startDateExpiry } = JSON.parse(startDateStr);
            const { value: endDateValue, expiry: endDateExpiry } = JSON.parse(endDateStr);

            if (now.getTime() > startDateExpiry || now.getTime() > endDateExpiry) {
                localStorage.removeItem('startDate');
                localStorage.removeItem('endDate');
                // Setze zusätzlichen State oder führe Aktionen aus, wenn Daten entfernt wurden
            } else {
                // Optional: Setze State mit den abgerufenen Daten, falls gültig
            }
        }
    };
    

    const pickerChange = (ev) => {
        let startdate = new Date(ev.value[0]);
        let endDate = new Date(ev.value[1]);
        console.log(startdate);
        console.log(endDate);
        // Konvertiere das Datum in das Format dd.mm.YYYY
        const formatDate = (date) => {
            let day = date.getDate();
            let month = date.getMonth() + 1; // Monate sind 0-basiert
            let year = date.getFullYear();
    
            // Füge eine führende 0 hinzu, falls Tag oder Monat < 10 sind
            day = day < 10 ? `0${day}` : day;
            month = month < 10 ? `0${month}` : month;
    
            return `${day}.${month}.${year}`;
        };

        const ttl = 1000 * 60 * 60; // 1 Stunde in Millisekunden
        const now = new Date();
    
        const formattedStartDate = formatDate(startdate);
        const formattedEndDate = formatDate(endDate);

        const startDateWithExpiry = JSON.stringify({ value: formattedStartDate, expiry: now.getTime() + ttl });
        const endDateWithExpiry = JSON.stringify({ value: formattedEndDate, expiry: now.getTime() + ttl });
        localStorage.setItem('startDate', startDateWithExpiry);
        localStorage.setItem('endDate', endDateWithExpiry);
        setShowConfirmButton(true);
        props.updateDateRange();
    };
    

    const changeDate = () => {
        props.hideDatepicker(false);

    }

    const pickerVisible = () => {
        
        props.hideDatepicker(false);
    }

    return (
        <>
            <Datepicker
                controls={['calendar']}
                select="range"
                calendarType="month"
                display="inline"
                touchUi={true}
                min={new Date()}
                max={twoYearsFromNow.setFullYear(twoYearsFromNow.getFullYear() + 2)}
                className={styles.inlineDatepicker}
                onChange={pickerChange}
                pages={calendarPages}
                showRangeLabels={false}
                invalid={invalidDays}
            />
            <div className={styles.buttonContainer}>
                {showConfirmButton && (
                    <Button variant="contained" color="primary" onClick={changeDate}>
                        Zeitraum wählen
                    </Button>
                )}
                {!showConfirmButton && (
                    <Button variant="contained" color="primary" onClick={pickerVisible}>
                        Abbrechen
                    </Button>
                )}
            </div>
        </>
    )
}

export default TrafeDatepicker;