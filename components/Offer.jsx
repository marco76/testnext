import React from 'react';
import { Grid, Paper } from '@mui/material';
import styles from "../styles/offer.module.scss";
import Button from '@mui/material/Button';


const Offer = () => {


    return (
        <Grid container spacing={2} className={styles.wrapper}>
            <Grid item xs={12} sm={4} className={styles.leftOffer}>
                <h4>Tag</h4>
                <h5>24 Stunden</h5>
                <h3>270 €</h3>
                <p>inklusive</p>
                <ul>
                    <li>150km pro Tag</li>
                    <li>ausführliche Einweisung</li>
                    <li>Vollkaskoversicherung</li>
                    <li>günsrige Kilometerpakete</li>
                </ul>
                <Button variant="contained" color="primary">
                    jetzt buchen
                </Button>
            </Grid>
            <Grid item  xs={12} sm={4} className={styles.middleOffer}>
            <h4>Wochenende</h4>
                <h5>Samstag bis Montag</h5>
                <h3>750 €</h3>
                <p>inklusive</p>
                <ul>
                    <li>150km pro Tag</li>
                    <li>ausführliche Einweisung</li>
                    <li>Vollkaskoversicherung</li>
                    <li>günsrige Kilometerpakete</li>
                </ul>
                <Button variant="contained" color="primary">
                    jetzt buchen
                </Button>
            </Grid>
            <Grid item  xs={12} sm={4} className={styles.rightOffer}>
                <h4>Woche</h4>
                <h5>über 1000km inklusive</h5>
                <h3>1700 €</h3>
                <p>inklusive</p>
                <ul>
                    <li>150km pro Tag</li>
                    <li>ausführliche Einweisung</li>
                    <li>Vollkaskoversicherung</li>
                    <li>günsrige Kilometerpakete</li>
                </ul>
                <Button variant="contained" color="primary">
                    jetzt buchen
                </Button>
            </Grid>
        </Grid>
    )
}

export default Offer