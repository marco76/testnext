import React from 'react';
import { Grid } from '@mui/material';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronRight, faChevronLeft, faMap } from '@fortawesome/pro-light-svg-icons';
import config from '@/config';
import Link from 'next/link';


import styles from "../styles/location.module.scss";

const Location = () => {

    return (
        <div className={styles.container}>
            <h4>Abholung</h4>
            <Grid container spacing={2} className={styles.firstAddress}>
                <Grid item xs={3} className={styles.IconWrapper}>
                    <FontAwesomeIcon icon={faChevronRight} />
                </Grid>
                <Grid item xs={9}>
                    <p>{`${config.street}`}</p>
                    <p>{`${config.city}`}</p>

                </Grid>
            </Grid>
            <h4>Rückgabe</h4>
            <Grid container spacing={2}>
                <Grid item xs={3} className={styles.IconWrapper}>
                    <FontAwesomeIcon icon={faChevronLeft} />
                </Grid>
                <Grid item xs={9}>
                    <p>{`${config.street}`}</p>
                    <p>{`${config.city}`}</p>

                </Grid>
                <Grid item xs={12} className={styles.mapsWrapper}>
                    <Link href={`${config.mapsLink}`} alt={`${config.firmname}` + 'auf Google Maps'} target='_blank'>
                        <FontAwesomeIcon icon={faMap} className={styles.googleMaps} /> auf Google Maps anzeigen
                    </Link>
                </Grid>
            </Grid>
        </div>
    )
}

export default Location;