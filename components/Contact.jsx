import React from 'react';
import { Grid } from '@mui/material';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPhone } from '@fortawesome/pro-light-svg-icons';
import styles from "../styles/contact.module.scss";
import config from '@/config';
import Link from 'next/link';

const Contact = () => {

    return (
        <div className={styles.container}>
            <h4>Du hast Fragen?</h4>
            <Grid container spacing={2}>
                <Grid item xs={3} className={styles.IconWrapper}>
                    <FontAwesomeIcon icon={faPhone} />
                </Grid>
                <Grid item xs={9}>
                    <p>Wir helfen Dir gerne weiter! Kontaktiere uns telefonisch unter</p>
                    <Link href={'tel:' + `${config.hotlineLink}`} alt='Unsere Hotline'>
                        <h2>{`${config.hotline}`}</h2>
                    </Link>
                </Grid>
            </Grid>
        </div>
    )
}

export default Contact;