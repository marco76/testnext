import React from 'react';
import Grid from '@mui/material/Grid';
import Image from 'next/image';
import styles from "../styles/caroverview.module.scss";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck, faGauge, faUsers, faGasPump, faGears, faChevronRight } from '@fortawesome/pro-light-svg-icons';
import Button from '@mui/material/Button';
import Link from 'next/link';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';

export default function CarOverview(props) {

    const theme = useTheme();
    const isXs = useMediaQuery(theme.breakpoints.down('xs'));
    const isSm = useMediaQuery(theme.breakpoints.down('sm'));

    const spacing = isXs ? 0 : isSm ? 1 : 2;

    const beautifyUrl = (text) => {
        return text
            .toLowerCase()
            .replace(/ä/g, 'ae')
            .replace(/ö/g, 'oe')
            .replace(/ü/g, 'ue')
            .replace(/ß/g, 'ss')
            .replace(/\s+/g, '-')
            .replace(/[^a-z0-9-]/g, '');
    };

    return (
        <Grid container spacing={spacing}>
            {props.cars.map((vehicle) => (
                <Link
                    href={`/fuhrpark/${beautifyUrl(vehicle.producer)}-${beautifyUrl(vehicle.vehicletype)}?carid=${vehicle.carid}`}
                    passHref key={vehicle.carid} 
                >
                    <Grid item xs={12} className={styles.singleCar}>
                        <Grid container spacing={spacing} className={styles.carInternGridWrapper}>
                            <Grid item xs={12} sm={7} className={styles.imageContainer}>
                                <h3>{vehicle.type}</h3>
                                <Image src={vehicle.imagepath} alt={`${vehicle.producer} ${vehicle.vehicletype}`} width={700} height={475} className={styles.singleImage} />
                                <h2>{vehicle.producer} {vehicle.vehicletype}</h2>

                            </Grid>
                            <Grid item xs={12} sm={5} className={styles.advantageList}>
                                <ul>
                                    <li><FontAwesomeIcon icon={faCheck} /> Vollkasko-Versicherung inklusive</li>
                                    <li><FontAwesomeIcon icon={faCheck} /> 24/7 Kundenservice-Hotline</li>
                                    <li><FontAwesomeIcon icon={faCheck} /> Kilometer-Pakete zubuchbar</li>
                                    <li><FontAwesomeIcon icon={faCheck} /> Führerschein: B</li>
                                    <li><FontAwesomeIcon icon={faCheck} /> Fahrzeug sofort verfügbar</li>
                                </ul>
                            </Grid>
                            <Grid item xs={12} sm={7} className={styles.keyFact}>
                                <Grid container spacing={0}>
                                    <Grid item xs={3} sm={3} className={styles.singlFact}>
                                        <div className={styles.trafefacticon}>
                                            <FontAwesomeIcon icon={faGauge} />
                                        </div>
                                        {vehicle.hp}
                                    </Grid>
                                    <Grid item xs={3} sm={3} className={styles.singlFact}>
                                        <div className={styles.trafefacticon}>
                                            <FontAwesomeIcon icon={faUsers} />
                                        </div>
                                        {vehicle.seats}
                                    </Grid>
                                    <Grid item xs={3} sm={3} className={styles.singlFact}>
                                        <div className={styles.trafefacticon}>
                                            <FontAwesomeIcon icon={faGasPump} />
                                        </div>
                                        {vehicle.fuel}
                                    </Grid>
                                    <Grid item xs={3} sm={3} className={styles.singlFact}>
                                        <div className={styles.trafefacticon}>
                                            <FontAwesomeIcon icon={faGears} />
                                        </div>
                                        {vehicle.gear}
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs={12} sm={5} className={styles.carButton}>
                                <Button
                                    variant="contained" // Für einen Button mit Hintergrundfarbe
                                    color="primary" // Wähle die Farbe des Buttons
                                    startIcon={<FontAwesomeIcon icon={faChevronRight} />} // Setze das Icon links vom Text
                                >
                                    Zum Fahrzeug
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Link>
            ))}
        </Grid>
    );
}
