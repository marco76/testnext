/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['api.trafe.io', 'api.simplii.de'], // Füge die Domain hier hinzu
  },
};

export default nextConfig;
