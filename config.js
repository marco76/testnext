const config = {
  apiUrl: 'https://api.trafe.io',
  //apiUrl: 'https://api.simplii.de',
  firmname: 'trafe Autovermietung',
  hotline: '0841 / 938 969 50',
  hotlineLink: '+84193896950',
  street: 'Carl-Benz-Ring 4-6',
  city: '85080 Gaimersheim',
  mapsLink: 'https://www.google.de/maps/place/simplii+RENT+%7C+Autovermietung+Gaimersheim+%7C+24%2F7+erreichbar!/@48.7814344,11.3811027,17z/data=!3m2!4b1!5s0x479efddfd45e04dd:0x564f949bcf005b2f!4m6!3m5!1s0x479efd713db1f0b3:0x758af8c57bda684a!8m2!3d48.7814309!4d11.3836776!16s%2Fg%2F11tfdcd313?entry=ttu',
  teamname: 'trafe Team',
  mailaddress: 'info@trafe.io'

};

export default config;
