import axios from 'axios';
import { useRouter } from 'next/router';
import DashboardLayout from '@/components/admin_layout/layout';
import { CircularProgress, FormControl, InputLabel, Select, MenuItem, Grid } from '@mui/material';
import '@mobiscroll/react/dist/css/mobiscroll.min.css';
import { Eventcalendar, setOptions, localeDe, Button, formatDate, Popup, Toast } from '@mobiscroll/react';
import { useEffect, useMemo, useState, useCallback, useRef, } from 'react';
import config from '@/config';
import { fetchCustomerData } from '../api/fetchCustomerData';
import styles from "../../styles/dispo.module.scss";

setOptions({
    locale: localeDe,
    theme: 'ios',
    themeVariant: 'light'
});

export default function Dispo() {
    const router = useRouter();
    const [resources, setResources] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [events, setEvents] = useState([]);
    const [selectedProducer, setSelectedProducer] = useState('');
    const [selectedCartype, setSelectedCartype] = useState('');
    const producers = useMemo(() => [...new Set(resources.map(resource => resource.producer))], [resources]);
    const cartypes = useMemo(() => [...new Set(resources.map(resource => resource.cartype))], [resources]);
    const [isOpen, setOpen] = useState(false);
    const [anchor, setAnchor] = useState(null);
    const [currentEvent, setCurrentEvent] = useState(null);
    const [closeOnOverlay, setCloseOnOverlay] = useState(false);
    const [time, setTime] = useState('');
    const [header, setHeader] = useState('');
    const [bgColor, setBgColor] = useState('');
    const [isToastOpen, setToastOpen] = useState(false);
    const [toastText, setToastText] = useState();
    const timerRef = useRef(null);
    const [customerName, setCustomerName] = useState('');
    const [customerStreet, setCustomerStreet] = useState('');
    const [customerlocation, setCustomerLocation] = useState('');
    const [timeLabel, setTimeLabel] = useState('');
    const [carname, setCarname] = useState('');
    const [cattype, setCartype] = useState('');
    const [customerPhone, setCustomerPhone] = useState('');
    const [customerMail, setCustomerMail] = useState('');
    const [carid, setCarId] = useState('');


    const filteredResources = useMemo(() => resources.filter(resource =>
        (selectedProducer ? resource.producer === selectedProducer : true) &&
        (selectedCartype ? resource.cartype === selectedCartype : true)
    ), [resources, selectedProducer, selectedCartype]);

    const myView = useMemo(() => ({
        timeline: {
            type: 'month',
            startDay: 1,
            endDay: 5,
            eventList: true,
            weekNumbers: false
        },
    }), []);

    function convertDateFormat(dateStr) {
        const parts = dateStr.split(".");
        if (parts.length === 3) {
            return `${parts[2]}-${parts[1].padStart(2, '0')}-${parts[0].padStart(2, '0')}`;
        } else {
            console.error("Ungültiges Datumsformat. Erwartetes Format: DD.MM.YYYY");
            return null;
        }
    }

    useEffect(() => {
        const isLoggedIn = localStorage.getItem('isLoggedIn');
        if (!isLoggedIn) {
            router.push('/trafe-admin');
        }
    }, [router]);

    useEffect(() => {
        setIsLoading(true);
        axios.get(`${config.apiUrl}/cars/allcars/`)
            .then(response => {
                const data = response.data;
                const transformedResources = data.map(car => ({
                    id: car.carid,
                    name: `${car.vehicletype}`,
                    producer: car.producer,
                    cartype: car.type,
                    licenseplate: car.licenseplate,
                    color: '#ff4600',
                    rentalduration: car.rentalduration,
                    status: car.status,
                }));
                setResources(transformedResources);

                const specialEvents = transformedResources.filter(car => car.rentalduration === 'Langzeit' || car.status === 'Wartung').map(car => {
                    const currentYear = new Date().getFullYear();
                    let eventColor = '#ff4600';

                    if (car.status === 'Wartung') {
                        eventColor = '#ff0000';
                    }

                    return {
                        id: `special-${car.id}`,
                        resource: car.id,
                        start: new Date(`${currentYear}-01-01T00:00`),
                        end: new Date(`${currentYear}-12-31T23:59`),
                        text: car.rentalduration === 'Langzeit' ? `Langzeitmiete ${car.name}` : `Wartung ${car.name}`,
                        color: eventColor,
                        tooltip: false,
                        dragdrop: false,
                        dragdropErrorText: 'Termine mit Langzeitmiete oder Wartung können in der Dispo nicht verschoben werden!'
                    };
                });

                setEvents(specialEvents);
            })
            .catch(error => {
                console.error('Fehler beim Laden der Fahrzeuge:', error);
            })
            .finally(() => setIsLoading(false));
    }, []);

    useEffect(() => {
        const fetchEventsForCar = async (car) => {
            const url = `${config.apiUrl}/booking/opencarbindingrequests/${car.id}`;
            try {
                const response = await axios.get(url);
                const bookings = response.data; // Annahme, dass dies ein Array ist


                if (!Array.isArray(bookings) || bookings.length === 0) {
                    return [];
                }

                const eventsWithCustomerData = await Promise.all(bookings.map(async (booking) => {
                    const customerData = await fetchCustomerData(booking.kunden);
                    const uebergabedatum = convertDateFormat(booking.uebergabedatum);
                    const rueckgabedatum = convertDateFormat(booking.rueckgabedatum);
                    if (!uebergabedatum || !rueckgabedatum || !customerData) {
                        return null;
                    }

                    return {
                        id: booking.verbindlicheanfragenid,
                        resource: car.id,
                        start: new Date(uebergabedatum + 'T' + booking.uebergabeuhrzeit),
                        end: new Date(rueckgabedatum + 'T' + booking.rueckgabeuhrzeit),
                        text: `Anfrage ${booking.verbindlicheanfragenid}`,
                        color: '#00ff00',
                        customer: customerData,
                        timelabel: 'Anfragezeitraum',
                        header: `Buchung ${booking.bookingid}`,
                        customername: `${customerData.firstname} ${customerData.lastname}`,
                        customerphone: `${customerData.phone}`,
                        customermail: `${customerData.mail}`,
                        customerstreet: `${customerData.street} ${customerData.housenumber}`,
                        customerlocation: `${customerData.plz} ${customerData.location}`,
                        bookedcar: `${car.producer} ${car.name}`,
                        bookedcartype: car.cartype,
                        carid: car.id,
                        tooltip: true,
                        dragdrop: true
                    };
                }));

                return eventsWithCustomerData.filter(event => event !== null);

            } catch (error) {
                console.error(`Fehler beim Laden der Termine für das Fahrzeug: ${car.id}`, error);
                return [];
            }
        };

        const loadEventsForCars = async () => {
            const filteredCars = resources.filter(car => car.status !== 'Wartung' && car.rentalduration !== 'Langzeit');
            const eventsPromises = filteredCars.map(fetchEventsForCar);
            const eventsArrays = await Promise.all(eventsPromises);
            const validEvents = eventsArrays.flat().filter((event, index, self) =>
                index === self.findIndex((t) => (
                    t.id === event.id
                ))
            );

            setEvents(prevEvents => {

                const uniqueNewEvents = validEvents.filter(newEvent => !prevEvents.some(event => event.id === newEvent.id));
                return [...prevEvents, ...uniqueNewEvents];
            });
        };

        if (resources.length > 0) {
            loadEventsForCars();
        }
    }, [resources]);

    useEffect(() => {
        const fetchBookingEventsForCar = async (car) => {
            const url = `${config.apiUrl}/booking/getlastcarbooking/${car.id}`;

            try {
                const response = await axios.get(url);
                let bookings = response.data;
                if (!Array.isArray(bookings)) {
                    bookings = [bookings];
                }

                if (bookings.length === 0) {
                    return [];
                }

                const eventsWithCustomerData = await Promise.all(bookings.map(async (booking) => {
                    const customerData = await fetchCustomerData(booking.kunden);

                    if (!booking.uebergabedatum || !booking.rueckgabedatum || !customerData) {
                        return null;
                    }

                    return {
                        id: booking.bookingid,
                        resource: car.id,
                        start: new Date(booking.uebergabedatum + 'T' + booking.uebergabeuhrzeit),
                        end: new Date(booking.rueckgabedatum + 'T' + booking.rueckgabeuhrzeit),
                        text: `Buchung ${booking.bookingid}: ${customerData.firstname} ${customerData.lastname}`,
                        color: '#c048c0',
                        customer: customerData,
                        timelabel: 'Buchungszeitraum',
                        header: `Buchung ${booking.bookingid}`,
                        customername: `${customerData.firstname} ${customerData.lastname}`,
                        customerphone: `${customerData.phone}`,
                        customermail: `${customerData.mail}`,
                        customerstreet: `${customerData.street} ${customerData.housenumber}`,
                        customerlocation: `${customerData.plz} ${customerData.location}`,
                        bookedcar: `${car.producer} ${car.name}`,
                        bookedcartype: car.cartype,
                        carid: car.id,
                        tooltip: true,  
                        dragdrop: true
                    };
                }));

                return eventsWithCustomerData.filter(event => event !== null);
            } catch (error) {
                if (error.response && error.response.status === 404) {
                    return [];
                } else {
                    console.error('Fehler beim Abrufen der Booking-Events:', error);
                    return [];
                }
            }
        };

        const loadBookingsForCars = async () => {
            const filteredCars = resources.filter(car => car.status !== 'Wartung' && car.rentalduration !== 'Langzeit');
            const eventsPromises = filteredCars.map(fetchBookingEventsForCar);
            const eventsArrays = await Promise.all(eventsPromises);
            const validEvents = eventsArrays.flat().filter((event, index, self) =>
                index === self.findIndex((t) => (
                    t.id === event.id
                ))
            );

            setEvents(prevEvents => {
                const uniqueNewEvents = validEvents.filter(newEvent => !prevEvents.some(event => event.id === newEvent.id));
                return [...prevEvents, ...uniqueNewEvents];
            });
        }

        if (resources.length > 0) {
            loadBookingsForCars();
        }

    }, [resources]);

    const openTooltip = useCallback((args, closeOption) => {
        const event = args.event;
        const resource = filteredResources.find((car) => car.id === event.resource);
        const time = formatDate('hh:mm A', new Date(event.start)) + ' - ' + formatDate('hh:mm A', new Date(event.end));
        console.log(event);
        setCurrentEvent(event);

        setCustomerName(event.customername),
            setCustomerStreet(event.customerstreet),
            setCustomerLocation(event.customerlocation),
            setTimeLabel(event.timelabel),
            setCarname(event.bookedcar),
            setCartype(event.bookedcartype)
        setCarId(event.carid)
        setCustomerPhone(event.customerphone)
        setCustomerMail(event.customermail)

        setHeader(event.header);
        setBgColor(event.color);

        setTime(time);

        if (timerRef.current) {
            clearTimeout(timerRef.current);
        }

        setAnchor(args.domEvent.currentTarget || args.domEvent.target);
        setCloseOnOverlay(closeOption);
        setOpen(true);
    }, []);

    const handleEventHoverIn = useCallback(
        (args) => {
            const event = args.event;
            if (event.tooltip) {
            openTooltip(args, false);
            }
        },
        [openTooltip],
    );

    const handleEventHoverOut = useCallback(() => {
        timerRef.current = setTimeout(() => {
            setOpen(false);
        }, 200);
    }, []);

    const handleEventClick = useCallback(
        (args) => {
            const event = args.event;

            if (event.tooltip) {
                if (!isOpen) {
                    openTooltip(args, true);
                }
            }
        },
        [openTooltip, isOpen],
    );

    const handleMouseEnter = useCallback(() => {
        if (timerRef.current) {
            clearTimeout(timerRef.current);
        }
    }, []);

    const handleMouseLeave = useCallback(() => {
        timerRef.current = setTimeout(() => {
            setOpen(false);
        }, 200);
    }, []);

    const handleToastClose = useCallback(() => {
        setToastOpen(false);
    }, []);

    const showToast = useCallback((message) => {
        setToastText(message);
        setToastOpen(true);
    }, []);

    const setStatusButton = useCallback(() => {
        setOpen(false);
        const index = events.findIndex((item) => item.id === currentEvent.id);
        const newApp = [...events];
        newApp[index].confirmed = !events[index].confirmed;
        setAppointments(newApp);
        showToast('Appointment ' + (currentEvent.confirmed ? 'confirmed' : 'canceled'));
    }, [events, currentEvent, showToast]);

    const viewFile = useCallback(() => {
        setOpen(false);
        showToast('View file');
    }, [showToast]);

    const deleteApp = useCallback(() => {
        setAppointments(events.filter((item) => item.id !== currentEvent.id));
        setOpen(false);
        showToast('Appointment deleted');
    }, [events, currentEvent, showToast]);

    const handleEventUpdate = useCallback((event, inst) => {
        // Zugriff auf die Ereignisdaten, um zu prüfen, ob es sich um Wartung oder Langzeitmiete handelt
        const eventData = events.find(e => e.id === event.event.id);
        if (!eventData.dragdrop) {
            showToast(eventData.dragdropErrorText);
            return false; // Verhindert das Update
        }
        // Führen Sie hier Ihre Logik durch, um das Ereignis zu aktualisieren, wenn es nicht verhindert wurde
    }, [events]);


    if (isLoading) {
        return (
            <DashboardLayout>
                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
                    <CircularProgress />
                </div>
            </DashboardLayout>
        );
    }

    return (
        <DashboardLayout>
            <div style={{ padding: '20px' }}>
                <Grid container spacing={2}>
                    <Grid item xs={3}>
                        <FormControl fullWidth>
                            <InputLabel id="producer-select-label">Produzent</InputLabel>
                            <Select
                                labelId="producer-select-label"
                                id="producer-select"
                                value={selectedProducer}
                                label="Produzent"
                                onChange={e => setSelectedProducer(e.target.value)}
                            >
                                <MenuItem value="">
                                    <em>Alle</em>
                                </MenuItem>
                                {producers.map(producer => (
                                    <MenuItem key={producer} value={producer}>{producer}</MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>

                    <Grid item xs={3}>
                        <FormControl fullWidth>
                            <InputLabel id="cartype-select-label">Fahrzeugtyp</InputLabel>
                            <Select
                                labelId="cartype-select-label"
                                id="cartype-select"
                                value={selectedCartype}
                                label="Fahrzeugtyp"
                                onChange={e => setSelectedCartype(e.target.value)}
                            >
                                <MenuItem value="">
                                    <em>Alle</em>
                                </MenuItem>
                                {cartypes.map(cartype => (
                                    <MenuItem key={cartype} value={cartype}>{cartype}</MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                </Grid>
            </div>

            {isLoading ? (
                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
                    <CircularProgress />
                </div>
            ) : (
                <div>
                    <Eventcalendar
                        clickToCreate={true}
                        dragToCreate={true}
                        dragToMove={true}
                        dragToResize={true}
                        eventDelete={true}
                        showEventTooltip={true}
                        view={myView}
                        data={events}
                        resources={filteredResources}
                        renderResource={(resource, index, label) => (
                            <div>
                                <div style={{ fontWeight: 'bold', fontSize: '12px' }}>{resource.name}</div>
                                <div style={{ fontWeight: 'normal', fontSize: '10px' }}>{resource.licenseplate}</div>
                            </div>
                        )}
                        onEventHoverIn={handleEventHoverIn}
                        onEventHoverOut={handleEventHoverOut}
                        onEventClick={handleEventClick}
                        onEventUpdate={handleEventUpdate}
                    />
                    <Popup
                        display="anchored"
                        isOpen={isOpen}
                        anchor={anchor}
                        touchUi={false}
                        showOverlay={false}
                        contentPadding={false}
                        closeOnOverlayClick={closeOnOverlay}
                        width={350}
                        cssClass="md-tooltip"

                    >
                        <div onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave}>
                            <div className={styles.tooltipHeader} style={{ backgroundColor: bgColor }}>
                                {header}
                            </div>
                            <div className={styles.tooltipBody}>
                                <p><span>{timeLabel}</span>{time}</p>
                                <p><span>Fahrzeug ID: </span>{carid}</p>
                                <p><span>Fahrzeug</span>{carname}</p>
                                <p><span>Fahrzeugklasse: </span>{cattype}</p>
                                <p><span>Kundename: </span>{customerName}</p>
                                <p><span>Adresse</span>{customerStreet}<br />{customerlocation}</p>
                                <p><span>Telefon: </span>{customerPhone}</p>
                                <p><span>E-Mailadresse: </span>{customerMail}</p>
                                <Button color="success" variant="outline" className="md-tooltip-delete-button">
                                    Bearbeiten
                                </Button>
                                <Button color="danger" variant="outline" className="md-tooltip-delete-button">
                                    Löschen
                                </Button>
                            </div>

                        </div>
                    </Popup>
                    <Toast message={toastText} isOpen={isToastOpen} onClose={handleToastClose} />
                </div>
            )}
        </DashboardLayout>
    );
}
