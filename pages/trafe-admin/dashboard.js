import { useRouter } from 'next/router';
import { useEffect } from 'react';
import Button from '@mui/material/Button';
import DashboardLayout from '@/components/admin_layout/layout';
import { Typography } from '@mui/material';


export default function Dashboard() {
    const router = useRouter();

    useEffect(() => {
        // Prüft den Authentifizierungsstatus bei jedem Rendering der Komponente
        const isLoggedIn = localStorage.getItem('isLoggedIn');
        if (!isLoggedIn) {
            router.push('/trafe-admin');
        }
    }, [router]);

    // Logout Funktion
    const handleLogout = () => {
        // Entfernt den Login-Status
        localStorage.removeItem('isLoggedIn');
        // Leitet den Benutzer zur Login-Seite um
        router.push('/trafe-admin');
    };

    return (

        <DashboardLayout>
          
            <h1>Dashboard</h1>
            {/* Weitere Dashboard-Inhalte hier */}

            {/* Logout-Button */}
            <Button variant="contained" color="primary" onClick={handleLogout}>
                Logout
            </Button>
        </DashboardLayout>

    );
}
