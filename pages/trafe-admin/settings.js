import { useRouter } from 'next/router';
import { useEffect } from 'react';
import Button from '@mui/material/Button';
import DashboardLayout from '@/components/admin_layout/layout';
import { Typography } from '@mui/material';


export default function Settings() {
    const router = useRouter();

    useEffect(() => {
        // Prüft den Authentifizierungsstatus bei jedem Rendering der Komponente
        const isLoggedIn = localStorage.getItem('isLoggedIn');
        if (!isLoggedIn) {
            router.push('/trafe-admin');
        }
    }, [router]);



    return (

        <DashboardLayout>
          
            <h1>Content</h1>
     
        </DashboardLayout>

    );
}
