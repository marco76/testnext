// fetchCustomerData.js
import axios from 'axios';
import config from '@/config'; // Pfad zu deiner Config-Datei anpassen

export const fetchCustomerData = async (customerId) => {
    const url = `${config.apiUrl}/customer/findcustomerbyid/${customerId}`;
    try {
        const response = await axios.get(url);
        const customer = response.data;
        return {
            firstname: customer.firstname,
            lastname: customer.lastname,
            street: customer.street,
            housenumber: customer.housenumber,
            plz: customer.plz,
            location: customer.location,
            mail: customer.email,
            phone: customer.phonenumber
        };
    } catch (error) {
        console.error('Fehler beim Abrufen der Kundendaten:', error);
        return null;
    }
};
