import axios from "axios";

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}


export default function handler(req, res) {

    const formData = req.body;
    const sendCarRequest = async () => {
        let customerID = await checkUserIdEsxists();
        let aktuellesDatum = new Date();

        let stunde = aktuellesDatum.getHours();
        let minute = aktuellesDatum.getMinutes();
        let sekunde = aktuellesDatum.getSeconds();
        stunde = (stunde < 10) ? '0' + stunde : stunde;
        minute = (minute < 10) ? '0' + minute : minute;
        sekunde = (sekunde < 10) ? '0' + sekunde : sekunde;

        let tag = aktuellesDatum.getDate();
        let monat = aktuellesDatum.getMonth() + 1; // Da getMonth() 0-basiert ist
        let jahr = aktuellesDatum.getFullYear();

        tag = (tag < 10) ? '0' + tag : tag;
        monat = (monat < 10) ? '0' + monat : monat;

        if (customerID === null) {
            customerID = await createNewCustomerID();
            await insertNewCustomer(customerID);
        }

        const formatiertesDatum = tag + '.' + monat + '.' + jahr;

        const postdata = {
            fahrzeug: formData.carid,
            kunden: customerID,
            anfragedatum: formatiertesDatum,
            anfrageuhrzeit: stunde + ':' + minute + ':' + sekunde,
            uebergabedatum: formData.abholdatum,
            uebergabeuhrzeit: formData.abholzeit,
            rueckgabedatum: formData.rueckgabedatum,
            rueckgabeuhrzeit: formData.rueckgabezeit,
            mietdauer: 'Kurzzeit',
            netto: '',
            brutto: '',
            adblueaufpreis: '1,50',
            endbrutto: '',
            kilometerinklusive: formData.benoetigteKilometer,
            mehrkilometer: '',
            kautionsart: '',
            kautionbetrag: '',
            kanal: 'Website',
            nachweisName: '',
            nachweisPfad: '',
            status: 'Offen',
            anfragemitarbeiter: 'Buchungsstrecke',
            note: formData.bemerkungen
        }



        try {
            const response = await axios.post('https://api.trafe.io/booking/newrequest', postdata);
        } catch (error) {
            console.log(error)
        }
    }

    async function createNewCustomerID() {
        try {
            const response = await axios('https://api.trafe.io/customer/lastcustomerid');
            return response.data[0].customerid + 1;
        } catch (error) {
            return null;
        }
    }

    async function insertNewCustomer(customer_id) {

        const postdata = {
            customerid: customer_id,
            firstname: capitalizeFirstLetter(formData.vorname),
            lastname: capitalizeFirstLetter(formData.name),
            business: formData.firma,
            street: '',
            housenumber: '',
            plz: '',
            location: '',
            phonenumber: formData.telefonnummer,
            email: formData.emailadresse,
            birthdate: '',
            birthplace: '',
            identdocument: '',
            identdocumentnumber: '',
            identdocumentdate: '',
            identdocumentplace: '',
            driverlicensenumber: '',
            driverlicenseclasses: '',
            driverlicensedate: '',
            driverlicenseplace: '',
            accountowner: '',
            bankinginstitution: '',
            iban: '',
            bic: '',
            mandatsreferenz: '',
            signdate: '',
        }

        try {
            const response = await axios.post('https://api.trafe.io/customer/newcustomer', postdata);

        } catch (error) {
            console.log(error)
        }
    }

    async function checkUserIdEsxists() {
        try {
            const response = await axios('https://api.trafe.io/customer/findcustomer/' + formData.emailadresse);
            return response.data.customerid;
        } catch (error) {
            return null;
        }
    }

    sendCarRequest();
    res.status(200).json({ result: 'request_ok' })
}

