import axios from 'axios';
import config from '@/config';

export async function fetchCarDatafromApp() {
  const response = await axios.get(`${config.apiUrl}/cars/allcars/`);
  return response.data;
}