import config from "@/config"; // Importiere die Konfigurationsdatei

export async function fetchVehicles() {
  const response = await fetch(`${config.apiUrl}/cars/allcars/`, {
    method: 'GET', // oder die entsprechende Methode, die deine API erfordert
    headers: {
      'Content-Type': 'application/json',
      // Füge hier weitere Header hinzu, die deine API erfordert, z.B. Authentifizierungstoken
    },
  });

  if (!response.ok) {
    throw new Error('Fehler beim Abrufen der Fahrzeuge');
  }

  const data = await response.json();
  return data;
}
