import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import axios from 'axios';
import config from '@/config';
import { Grid } from '@mui/material';
import CircularProgress from '@mui/material/CircularProgress';
import Contact from '@/components/Contact';
import Location from '@/components/Location';
import OrderCar from '@/components/OrderCar';
import styles from "../styles/order.module.scss";
import OrderForm from '@/components/OrderForm';

const Order = () => {
    const router = useRouter();
    const { carid } = router.query;
    const [fahrzeug, setFahrzeug] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState(null);



    useEffect(() => {
        const fetchCarData = async () => {
            if (!carid) return;
            setIsLoading(true);
            try {
                const response = await axios.get(`${config.apiUrl}/cars/findcar/${carid}`);
                if (response.status === 200 && response.data) {
                    setFahrzeug(response.data);
                } else {
                    // Fahrzeugdaten nicht gefunden oder leere Antwort, leite auf 404 um
                    router.push('/404');
                }
            } catch (err) {
                console.error('Fehler beim Laden der Fahrzeugdaten:', err);
                setError(true); // Setzt den Fehlerstatus
                router.push('/404'); // Leite bei einem Fehler auf 404 um
            } finally {
                setIsLoading(false);
            }
        };

        fetchCarData();
    }, [carid, router]);

    if (isLoading) return <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}><CircularProgress /></div>;
    if (error) return <div>{error}</div>;

    return (
        <div className={styles.mainWrapper}>
            <Grid container spacing={2} className={styles.container}>

                <Grid item xs={12} className={styles.introContainer}>
                    <h2>
                        Konditionen anfragen
                    </h2>
                    <p>
                        Bitte vervollständige Deine Angaben, damit wir Deine Anfrage bearbeiten können.
                    </p>
                </Grid>
                <Grid item xs={12} sm={8} className={styles.formContainer}>
                    <OrderForm carid={fahrzeug.carid} />
                </Grid>
                <Grid item xs={12} sm={4}  className={styles.rightWrapper}>
                    <Location />
                    <OrderCar car={fahrzeug} />
                    <Contact />
                </Grid>
            </Grid>
        </div>
    );
};

export default Order;
