import React, { useState, useEffect, useRef } from 'react';
import { useRouter } from 'next/router';
import axios from 'axios';
import config from '@/config';
import CircularProgress from '@mui/material/CircularProgress';
import Image from 'next/image';
import Link from 'next/link';
import styles from "../../styles/singledetail.module.scss";
import OtherCars from '@/components/OtherCars';
import ImageCarousel from '@/components/Carousel';
import Keyfacts from '@/components/Keyfacts';
import CarFacts from '@/components/CarFacts';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Button from '@mui/material/Button';
import Datepicker from '@/components/Datepicker';
import TextField from '@mui/material/TextField';
import Offer from '@/components/Offer';



const FahrzeugDetail = () => {
    const router = useRouter();
    const { carid } = router.query;
    const [fahrzeug, setFahrzeug] = useState(null);
    const [isLoading, setIsLoading] = useState(true); // Starte mit isLoading = true
    const [error, setError] = useState(null);
    const [isCarousel, setIsCarousel] = useState(true);
    const text = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.   Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer";
    const firstSentenceEndIndex = text.indexOf('.') + 1; // Finde das Ende des ersten Satzes
    const firstSentence = text.substring(0, firstSentenceEndIndex); // Der erste Satz
    const restOfText = text.substring(firstSentenceEndIndex); // Der Rest des Textes
    const [pickerStatus, setPickerStatus] = useState(false);
    const [dateRange, setDateRange] = useState('');
    const [theme, setTheme] = useState(createTheme({
        palette: {
            primary: {
                main: '#333',
            },
            secondary: {
                main: '#999',
            },
        },
        components: {
            // Konfiguration für MUI Button
            MuiButton: {
                styleOverrides: {
                    // Stile für die "root"-Variante des Buttons
                    root: {
                        color: 'white', // Setzt die Textfarbe auf Weiß
                    },
                },
            },
        },
    }));

    const carouselRef = useRef(null); // Erstellen der Ref

    const scrollToCarousel = () => {
        if (carouselRef.current) {
            carouselRef.current.scrollIntoView({ behavior: 'smooth', block: 'start' }); // Verwendet die ref, um zum Carousel zu scrollen
        }
    };

    // Konvertierungsfunktion: Hex zu RGB
    const hexToRgb = (hex) => {
        let r = 0, g = 0, b = 0;
        // 3 Zeichen lang oder 6 Zeichen lang
        if (hex.length === 4) {
            r = parseInt(hex[1] + hex[1], 16);
            g = parseInt(hex[2] + hex[2], 16);
            b = parseInt(hex[3] + hex[3], 16);
        } else if (hex.length === 7) {
            r = parseInt(hex[1] + hex[2], 16);
            g = parseInt(hex[3] + hex[4], 16);
            b = parseInt(hex[5] + hex[6], 16);
        }
        return [r, g, b];
    };

    useEffect(() => {
        fetch(`${config.apiUrl}/settings/design`)
            .then(response => response.json())
            .then(data => {
                const colors = data[0];
                const primaryColor = colors.primarycolorhex || '#1976d2';
                const secondaryColor = colors.secondcolorhex || '#dc004e';

                // Konvertiere primaryColor von Hex zu RGB
                const [r, g, b] = hexToRgb(primaryColor);

                const newTheme = createTheme({
                    palette: {
                        primary: {
                            main: primaryColor,
                        },
                        secondary: {
                            main: secondaryColor,
                        },
                    },
                    components: {
                        // Konfiguration für MUI Button
                        MuiButton: {
                            styleOverrides: {
                                // Stile für die "root"-Variante des Buttons
                                root: {
                                    color: 'white', // Setzt die Textfarbe auf Weiß
                                },
                            },
                        },
                    },
                });

                setTheme(newTheme);
                document.documentElement.style.setProperty('--primary-color', primaryColor);
                document.documentElement.style.setProperty('--secondary-color', secondaryColor);
                // Setze die primary Farbe mit Opazität
                document.documentElement.style.setProperty('--primary-color-opacity', `rgba(${r}, ${g}, ${b}, 0.1)`);
                document.documentElement.style.setProperty('--white', '#fff');
                document.documentElement.style.setProperty('--grey', '#ccc');
            })
            .catch(error => {
                console.error("Fehler beim Laden der Farben: ", error);
            });
    }, []);

    useEffect(() => {
        const fetchCarData = async () => {
            if (!carid) return; // Verhindert den API-Aufruf, bis carid verfügbar ist
            setIsLoading(true);
            try {
                const response = await axios.get(`${config.apiUrl}/cars/findcar/${carid}`);
                if (response.status === 200 && response.data) {
                    setFahrzeug(response.data);
                } else {
                    // Fahrzeugdaten nicht gefunden, leite auf die 404-Seite um
                    router.push('/404');
                }
            } catch (err) {
                console.error('Fehler beim Laden der Fahrzeugdaten:', err);
                setError('Fehler beim Laden der Fahrzeugdaten');
                // Bei einem Fehler beim Abrufen der Daten ebenfalls auf die 404-Seite umleiten
                router.push('/404');
            } finally {
                setIsLoading(false);
            }
        };
    
        fetchCarData();
    }, [carid, router]);
    
// Test

    useEffect(() => {
        updateDateRangeFromStorage();
    }, []);

    const updateDateRangeFromStorage = () => {
        const getValidDate = (item) => {
            if (item) {
                const { value, expiry } = JSON.parse(item);
                if (new Date().getTime() < expiry) {
                    return value;
                }
            }
            return null;
        };

        const storedStartDate = getValidDate(localStorage.getItem('startDate'));
        const storedEndDate = getValidDate(localStorage.getItem('endDate'));

        if (storedStartDate && storedEndDate) {
            setDateRange(`${storedStartDate} - ${storedEndDate}`);
        } else {
            setDateRange('');
        }
    };

    const handlePickerInputClick = () => {
        setPickerStatus(true);
    };

    const hideDatepicker = () => {
        setPickerStatus(false);
    }

    const updateDateRange = (startDate, endDate) => {
        const formattedStartDate = new Date(startDate).toLocaleDateString('de-DE');
        const formattedEndDate = new Date(endDate).toLocaleDateString('de-DE');
        setDateRange(`${formattedStartDate} - ${formattedEndDate}`);
        setPickerStatus(false); // Verbergen Sie den Datepicker nach der Auswahl
    };


    if (isLoading) return <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}><CircularProgress /></div>;
    if (error) return <div>{error}</div>;

    return (
        <ThemeProvider theme={theme}>
            {pickerStatus && (
                <div className={styles.datepickerLayer}>
                    <div className={styles.inner}>
                        <Datepicker hideDatepicker={hideDatepicker} updateDateRange={updateDateRangeFromStorage} />
                    </div>
                </div>
            )}

            {fahrzeug ? (
                <div className={styles.singleCarContainer}>
                    <div className={styles.imageContainer}>
                        <Image src={fahrzeug.imagepath} width={500} height={500} alt={fahrzeug.producer + ' ' + fahrzeug.vehicletype} />
                    </div>
                    <div className={styles.headline}>
                        <h1>{fahrzeug.producer} {fahrzeug.vehicletype} mieten</h1>
                    </div>
                    <div className={styles.pickerWrapper}>
                        <h4>Bitte wählen Sie einen Mietzeitraum</h4>
                        <div>
                            <TextField
                                label="Bitte wählen Sie einen Zeitraum"
                                variant="outlined"
                                onClick={handlePickerInputClick}
                                className={styles.pickerField}
                                value={dateRange}
                                InputProps={{
                                    readOnly: true,
                                }}
                            />
                        </div>
              
                        <Link href={'/fahrzeug-anfragen/?carid=' + fahrzeug.carid} alt={fahrzeug.producer + ' ' + fahrzeug.vehicletype + 'jetzt anfragen'}>
                            <Button variant="contained" color="primary">
                                {fahrzeug.producer + ' ' + fahrzeug.vehicletype} jetzt anfragen
                            </Button>
                        </Link>
                        <p className={styles.otherDetails} onClick={scrollToCarousel}>weitere Fahrzeugdetails</p> 
                    </div>
                    <div className={styles.offerWrapper}>
                        <Offer />
                    </div>
                    <div className={styles.headlineTwoContainer}>
                        <h2>Mieten Sie jetzt ein unvergessliches Fahrerlebniss!</h2>
                    </div>
                    {isCarousel && (
                        <div className={styles.carouselContainer} ref={carouselRef}> 
                            <ImageCarousel car={fahrzeug} setIsCarousel={setIsCarousel} />
                        </div>
                    )}
                    <div className={styles.descriptionWrapper}>
                        <h2>Headline</h2>
                        <p><strong>{firstSentence}</strong></p>
                        <p>{restOfText}</p>
                    </div>
                    <div className={styles.keyFacts}>
                        <Keyfacts carData={fahrzeug} />
                    </div>
                    <div className={styles.otherFacts}>
                        <h3>weitere Details</h3>
                        <CarFacts carData={fahrzeug} />
                    </div>
                    <div className={styles.buttonWrapper}>
                        <Link href={'/fahrzeug-anfragen/?carid=' + fahrzeug.carid} alt={fahrzeug.producer + ' ' + fahrzeug.vehicletype + 'jetzt anfragen'}>
                            <Button variant="contained" color="primary">
                                {fahrzeug.producer + ' ' + fahrzeug.vehicletype} jetzt anfragen
                            </Button>
                        </Link>
                    </div>

                    <div className={styles.otherCars}>
                        <h3>Entdecke weitere Fahrzeuge</h3>
                        <OtherCars currentCarId={carid} />
                    </div>
                </div>
            ) : (
                <div>Keine Fahrzeugdaten gefunden.</div>
            )
            }
        </ThemeProvider >
    );
};

FahrzeugDetail.getLayout = function getLayout(page) {
    return page;
}

export default FahrzeugDetail;
