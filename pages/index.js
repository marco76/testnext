import React, { useEffect, useState } from 'react';
import styles from "../styles/index.module.scss";
import { fetchCarDatafromApp } from './api/cars';
import Grid from '@mui/material/Grid';
import CircularProgress from '@mui/material/CircularProgress';
import FuhrparkFilter from '@/components/Filter';
import FuhrparkCars from '@/components/CarOverview';
import Datepicker from '@/components/Datepicker';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';

const generateFilterData = (data, keys) => {
    const uniqueValues = {};
    keys.forEach(key => {
        uniqueValues[key] = new Set();
    });

    data.forEach(item => {
        keys.forEach(key => {
            if (item[key]) {
                uniqueValues[key].add(item[key]);
            }
        });
    });

    Object.keys(uniqueValues).forEach(key => {
        uniqueValues[key] = Array.from(uniqueValues[key]);
    });

    return uniqueValues;
}

const Fuhrpark = () => {
    const [carData, setCarData] = useState(null);
    const [filteredCarData, setFilteredCarData] = useState([]);
    const [filterData, setFilterData] = useState([]);
    const [filterValues, setFilterValues] = useState({});
    const [isFilterVisible, setIsFilterVisible] = useState(false);
    const [pickerStatus, setPickerStatus] = useState(false);
    const [dateRange, setDateRange] = useState('');
    const [filterOpen, setFilterOpen] = useState(false);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const carDataArray = await fetchCarDatafromApp();
                const uniqueValues = generateFilterData(carDataArray, ['type', 'producer', 'gear', 'fuel', 'drivetype']);
                setFilterData(uniqueValues);
                setCarData(carDataArray);
            } catch (error) {
                console.error('Error fetching car data:', error);
            }
        };
        fetchData();
    }, []);

    const changeFilterOpen = () => {
        if(filterOpen) {
            setFilterOpen(false);
        } else {
            setFilterOpen(true);
        }
    }

    useEffect(() => {
        const applyFilters = () => {
            if (!carData) return;
            const filteredData = carData.filter(car => {
                return Object.entries(filterValues).every(([key, values]) => {
                    return values.includes(car[key]);
                });
            });
            setFilteredCarData(filteredData);
        };
        applyFilters();
    }, [carData, filterValues]);

    useEffect(() => {
        updateDateRangeFromStorage();
    }, []);

    const openFilter = () => {
        setIsFilterVisible(!isFilterVisible);
        document.body.style.overflow = isFilterVisible ? 'hidden' : 'auto';
    
    }


    const editFilter = (filterValue) => {
        const [key, value] = filterValue.split("-");
        setFilterValues(prevFilterValues => {
            const newFilterValues = { ...prevFilterValues };
            if (newFilterValues[key]?.includes(value)) {
                newFilterValues[key] = newFilterValues[key].filter(item => item !== value);
                if (newFilterValues[key].length === 0) {
                    delete newFilterValues[key];
                }
            } else {
                newFilterValues[key] = [...(newFilterValues[key] || []), value];
            }
            return newFilterValues;
        });
    };

    const handlePickerInputClick = () => setPickerStatus(true);
    const hideDatepicker = () => setPickerStatus(false);

    const updateDateRangeFromStorage = () => {
        const getValidDate = (item) => {
            if (item) {
                const { value, expiry } = JSON.parse(item);
                if (new Date().getTime() < expiry) {
                    return value;
                }
            }
            return null;
        };

        const storedStartDate = getValidDate(localStorage.getItem('startDate'));
        const storedEndDate = getValidDate(localStorage.getItem('endDate'));

        if (storedStartDate && storedEndDate) {
            setDateRange(`${storedStartDate} - ${storedEndDate}`);
        } else {
            setDateRange('');
        }
    };

    if (carData === null) {
        return (
            <div className={styles.main_container} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <CircularProgress />
            </div>
        );
    }

    return (
        <div className={styles.mainWrapper}>
        {pickerStatus && (
                <div className={styles.datepickerLayer}>
                    <div className={styles.inner}>
                        <Datepicker hideDatepicker={hideDatepicker} updateDateRange={updateDateRangeFromStorage} />
                    </div>
                </div>
            )}
            <div className={styles.main_container}>
                <Grid container spacing={0}>
                    <Grid item xs={12} className={styles.pickerInput}>
                        <TextField
                            label="Bitte wählen Sie einen Zeitraum"
                            variant="outlined"
                            onClick={handlePickerInputClick}
                            className={styles.pickerField}
                            value={dateRange}
                            InputProps={{ readOnly: true, }}
                        />
                    </Grid>
                    <Grid item xs={12} sm={12} md={4} className={`${styles.FilterContainer} ${isFilterVisible ? styles.filtervisible : ''}`}>
                        <FuhrparkFilter filterData={filterData} filterAction={editFilter} filterClose={setIsFilterVisible}/>
                    </Grid>
                    <Grid item xs={12} sm={12} md={8} className={styles.overviewCarWrapper}>
                        <FuhrparkCars cars={filteredCarData} openFilter={openFilter} />
                    </Grid>
             
                </Grid>
                <div className={styles.openFilterButtonWrapper}>
                        <Button variant="contained" color="primary" onClick={openFilter} className={styles.openFilterButton}>
                            Fahrzeuge filtern
                        </Button>
                    </div>
            </div>
        </div>
    );
};

export default Fuhrpark;
