import React from 'react';
import Link from 'next/link';
import Button from '@mui/material/Button';
import styles from "../styles/errorPage.module.scss";
import OtherCars from "@/components/OtherCars";

const ErrorPage = () => {
    return (
        <div className={styles.wrapper}>
            <h1>404</h1>
            <h2>Die Seite oder das Fahrzeug existiert nicht!</h2>
            <Link href="/" passHref>
                <Button variant="contained" color="primary">
                    Zurück zum Fuhrpark
                </Button>
            </Link>

            <OtherCars />
        </div>
    )
}

export default ErrorPage;